var net = require("net");
var JSONStream = require('JSONStream');

var serverHost = process.argv[2];
var serverPort = process.argv[3];
var botName = "The Marvellous Car";
var botKey = process.argv[5];

console.log("[INIT] I'm", botName, "and connect to", serverHost + ":" + serverPort);

client = net.connect(serverPort, serverHost, function() {
  return send({
    msgType: "join",
    data: {
      name: botName,
      key: botKey
    }
  });
});

function send(json) {
  console.log("[SEND] "+JSON.stringify(json));
  client.write(JSON.stringify(json));
  return client.write('\n');
};

// ZE Track
var Track = function(){
  this.init.apply( this , arguments )
};
Track.prototype = {

  pieces : null,

  lanes : null,

  init : function ( trackMessage ){
    this.pieces = trackMessage.pieces;
    this.lanes = trackMessage.lanes;

    // compute the true length of the piece, depending on the distance from center its radius
    for(var i=this.pieces.length;i--;){

      this.pieces[i].actualLength = [];

      for(var k=this.lanes.length;k--;){

        if( this.pieces[i].length ){
          // straight line
          this.pieces[i].actualLength[k] = this.pieces[i].length;
        }else{
          // curved line

          // compute the length of the piece
          var actualRadius = this.pieces[i].radius - this.lanes[k].distanceFromCenter;
          
          var radianAngle = this.pieces[i].angle / 180 * Math.PI;

          this.pieces[i].actualLength[k] = Math.abs(radianAngle * actualRadius);
        }
      }
    }
  },

};

// ZE Car
var Car = function(){
  this.init.apply( this , arguments )
};
Car.prototype = {

  speed : 0,

  distance : 0,

  angle : 0,

  piece : 0,

  lane : 0,

  init : function( carMessage ){
    this.name = carMessage.name;
    this.color = carMessage.color;
  },

  update : function( track, carMessage ){

    var dst;

    if( carMessage.piecePosition.pieceIndex != this.piece ){
      // go on the next piece
      dst = ( track.pieces[ this.piece ].actualLength[ this.lane ] - this.distance ) + carMessage.piecePosition.inPieceDistance;
    }else{
      dst = carMessage.piecePosition.inPieceDistance - this.distance;
    }

    this.distance = carMessage.piecePosition.inPieceDistance;

    this.angle = carMessage.angle;

    this.speed = dst / TIMESTEP;

    this.piece = carMessage.piecePosition.pieceIndex;

    this.lane = carMessage.piecePosition.lane.endLaneIndex;
  },

}

// ZE Constant
var TIMESTEP = 1/60 ;
var SPEEDMAX_CURVE_HARD = 350;
var SPEEDMAX_CURVE_SOFT = 500;
var SPEEDMAX_STRAIGHT = Infinity;
var DRIFT_MAX = 50.0 ;
var ANGLE_SOFT = 35.0 ;

// ZE World
var world = (function() {
  var track ;
  var myCar ;
  
  var started ;
  
  var throttle ;
  var speedMax ;
  var bestLane ;

  var compute = function() {
    // TODO compute best lane to go
    bestLane = myCar.lane;

    // TODO compute max speed to go
    if ( track.pieces[myCar.piece].length ) {
      speedMax = SPEEDMAX_STRAIGHT;
      if ( !track.pieces[(myCar.piece+1)%track.pieces.length].length ) {
        if ( Math.abs(track.pieces[(myCar.piece+1)%track.pieces.length].angle) > ANGLE_SOFT )
          speedMax = SPEEDMAX_CURVE_HARD;
        else
          speedMax = SPEEDMAX_CURVE_SOFT;
      }
    } else {
      if ( Math.abs(track.pieces[(myCar.piece+1)%track.pieces.length].angle) > ANGLE_SOFT )
        speedMax = SPEEDMAX_CURVE_HARD;
      else
        speedMax = SPEEDMAX_CURVE_SOFT;
    }

    // TODO compute throttle
    // TODO Regulateur PID : http://www.ferdinandpiette.com/blog/2011/08/implementer-un-pid-sans-faire-de-calculs/
    if ( myCar.angle > DRIFT_MAX ) {
      throttle = 0.1 ;
    } else if (myCar.speed > speedMax) {
      throttle = 0.3;
    } else {
      throttle = 1.0 ; 
    }
  };

  return {
    setCar:function(data){
      myCar = new Car(data) ;
    },

    init:function(data){
      track = new Track(data.race.track);
      started = false;
    },

    start:function(){
      console.log("[RACE] start")
      started = true ;
    }, 

    update:function(data){
      // update cars
      for (var i=0; i<data.length; i++) {
        if (myCar.color === data[i].id.color){
          myCar.update(track, data[i] );
          console.log("[CAR] speed:"+myCar.speed)
        } else { 
          // TODO update other car
        }
      }
      compute();
    },

    crash:function(data){
      console.log("[CRASH] "+JSON.stringify(data));
    },

    spawn:function(data){
      console.log("[SPAWN] "+JSON.stringify(data))
    },

    end:function(){
      console.log("[RACE] end")
      stated = false;
    },

    dnf: function(data) {
      console.log("[DNF] "+JSON.stringify(data))
    },

    act:function(){
      if (!started) {
        send({msgType:"ping", data:null})
        return;
      }

      if (myCar.lane != bestLane && track.pieces[myCar.piece].switch){
        //CHECK ME
        var direction = "Right";
        if ( currentLane < bestLane)
          direction = "Left";

        send({
          msgType:"switchLane",
          data:direction
        });
      } else {
        send({
          msgType:"throttle",
          data: throttle
        });
      }
    },

  }
}())


jsonStream = client.pipe(JSONStream.parse());

jsonStream.on('error', function() {
  return console.log("[ERROR] disconnected");
});

jsonStream.on('data', function(data) {
  console.log("[RECEIVE] "+JSON.stringify(data));
  switch(data.msgType){
  case 'join':
    break;
  case 'yourCar':
    world.setCar(data.data);
    break;
  case 'gameInit':
    world.init(data.data);
    break;
  case 'gameStart':
    world.start();
    break;
  case 'carPositions':
    world.update(data.data);
    break;
  case 'crash':
    world.crash(data.data);
    break;
  case 'spawn':
    world.spawn(data.data);
    break;
  case 'lapFinished':
    break;
  case 'finish':
    break;
  case 'dnf':
    world.dnf(data.data);
    break;
  case 'gameEnd':
    world.end();
    break;
  case 'tournamentEnd':
    break;
  default:
    console.log("[WARN] Unexpected message !");
  }

  world.act();

});
